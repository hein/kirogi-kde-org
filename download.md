---
layout: page
title: Download
sorted: 4
---

# Download

Kirogi development is still in its early stages. While already usable, the project has not issued a stable release yet.

Pre-release builds and source code are available below.

We expect developer- and enthusiast-oriented software distributions to pick up Kirogi soon. We'll update this page with additional links as we are informed of them.

## Nightly Development Builds

### Linux (Flatpak x86-64)

Nightly Flatpak builds for desktop Linux are available <a href="https://binary-factory.kde.org/job/Kirogi_flatpak/">here</a>.

### Android

Nightly Android APKs are available <a href="https://binary-factory.kde.org/job/Kirogi_android/">here</a>.

## Source Code

You can browse (and find <code>git clone</code> URLs for) Kirogi's source code <a href="https://invent.kde.org/kde/kirogi">here</a>.
